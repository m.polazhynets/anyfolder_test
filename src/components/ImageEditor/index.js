import React, { useRef, useState } from "react";
import { Row, Col, Button, Space } from "antd";
import CanvasDraw from "react-canvas-draw";
import html2canvas from "html2canvas";

const ImageEditor = () => {
  const inputRef = useRef(null);
  const canvasRef = useRef(null);
  const [imgSrc, setImgSrc] = useState("");

  const onChange = () => {
    let file = inputRef.current.files[0];
    let reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImgSrc(reader.result);
      canvasRef.current.drawImage();
    };
  };

  const saveFile = () => {
    if (!canvasRef.current) return;

    html2canvas(document.querySelector("#canvas")).then((canvas) => {
      let dataURL = canvas.toDataURL();
      let fileName = (Math.random() + 1).toString(36).substring(5);

      let images = localStorage.getItem("images") ?? "[]";
      let imagesArray = JSON.parse(images);

      imagesArray.push({
        fileName: fileName,
        fileUrl: dataURL,
      });

      localStorage.setItem("images", JSON.stringify(imagesArray));

      alert("File saved to gallery");
    });
  };

  return (
    <>
      <Row gutter={16}>
        <Col className="gutter-row" span={16}>
          <form>
            <Space size={"middle"}>
              <input ref={inputRef} type="file" onChange={onChange} />
            </Space>
          </form>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col className="gutter-row" span={16}>
          <div id="canvas" className="canvas-draw">
            <CanvasDraw
              ref={canvasRef}
              brushColor="red"
              hideInterface
              hideGrid
              brushRadius={5}
              imgSrc={imgSrc}
              canvasWidth={635}
              canvasHeight={400}
            />
          </div>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col className="gutter-row" span={16}>
          <Button onClick={saveFile} type="primary" size="large" shape="round">
            Take picture
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default ImageEditor;
