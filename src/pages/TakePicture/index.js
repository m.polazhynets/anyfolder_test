import React from "react";
import { Layout, Menu } from "antd";
import { ImageEditor } from "../../components";
// import styles from "./index.module.scss";

const { Sider, Content } = Layout;

const TakePicture = ({ history }) => {
  return (
    <Layout>
      <Sider breakpoint="lg" collapsedWidth="0">
        <Menu mode="inline" theme="light" defaultSelectedKeys={["1"]}>
          <Menu.Item onClick={() => history.push("/")} key="1">
            Take Picture
          </Menu.Item>
          <Menu.Item onClick={() => history.push("/picture-gallery")} key="2">
            Picture Gallery
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            <h1>Take Picture Page</h1>
            <p>
              enter any picture url, and load the picture to the square space.{" "}
            </p>

            <ImageEditor />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default TakePicture;
