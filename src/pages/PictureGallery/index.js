import React, { useState, useEffect } from "react";
import { Layout, Menu, Row, Col, Image, Button } from "antd";
// import styles from "./index.module.scss";

const { Sider, Content } = Layout;

const PictureGallery = ({ history }) => {
  const [imgs, setImg] = useState(null);

  useEffect(() => {
    let images = localStorage.getItem("images") ?? "[]";
    let imagesArray = JSON.parse(images);

    setImg(imagesArray);
  }, []);

  const deleteFile = (index) => {
    let tmpArray = imgs;

    tmpArray.splice(index, 1);

    let newImagesArray = JSON.parse(JSON.stringify(tmpArray));

    setImg(newImagesArray);

    localStorage.setItem("images", JSON.stringify(newImagesArray));
  };

  const downloadFile = (fileUrl, fileName) => {
    let downloadLink = document.createElement("a");

    downloadLink.setAttribute("download", `${fileName}.png`);

    downloadLink.href = fileUrl;
    downloadLink.click();
  };

  return (
    <Layout>
      <Sider breakpoint="lg" collapsedWidth="0">
        <Menu mode="inline" theme="light" defaultSelectedKeys={["2"]}>
          <Menu.Item onClick={() => history.push("/")} key="1">
            Take Picture
          </Menu.Item>
          <Menu.Item onClick={() => history.push("/picture-gallery")} key="2">
            Picture Gallery
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            <h1>Picture Gallery</h1>
            <p>Showing all pictures that been taking with the local storage.</p>

            {imgs &&
              imgs?.map((item, index) => (
                <Row gutter={16} key={index}>
                  <Col className="gutter-row" span={8}>
                    <Image width={200} src={item.fileUrl} />
                  </Col>
                  <Col className="gutter-row" span={8}>
                    <span>
                      {item.fileName}
                    </span>
                  </Col>
                  <Col className="gutter-row" span={8} align="end">
                    <Row gutter={16}>
                      <Col className="gutter-row" span={16}>
                        <Button
                          onClick={() => deleteFile(index)}
                          type="primary"
                        >
                          Delete
                        </Button>
                      </Col>
                    </Row>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={16}>
                        <Button
                          onClick={() =>
                            downloadFile(item.fileUrl, item.fileName)
                          }
                        >
                          Download
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              ))}
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default PictureGallery;
