import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { ScrollToTop } from "./components";
import { TakePicture, PictureGallery } from "./pages";
import history from "./utils/history";
import { Keys } from "./utils/keys";

import 'antd/dist/antd.css'; 
import "./styles/index.scss";

const PublicRoute = ({ component, ...rest }) => {
  return <Route {...rest} component={component} />;
};

const App = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Router history={history}>
      <ScrollToTop />
      <Switch>
        <PublicRoute exact path={Keys.TAKE_PICTURE} component={TakePicture} />
        <PublicRoute path={Keys.PICTURE_GALLERY} component={PictureGallery} />
      </Switch>
    </Router>
  );
};
export default App;
